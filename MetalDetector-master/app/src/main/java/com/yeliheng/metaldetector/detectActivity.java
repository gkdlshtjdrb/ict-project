package com.yeliheng.metaldetector;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.white.progressview.CircleProgressView;
import java.math.BigDecimal;


public class detectActivity extends AppCompatActivity implements SensorEventListener{
    final String TAG = "MetalDetector";
    private SensorManager sensorManager;
    private CircleProgressView progressView;
    private TextView metalState;
    private LineCharts lineCharts = new LineCharts();
    private Toolbar toolbar;
    double alarmLim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detect);
        initFunc();
        if(new Common().readSharedPreferencesByBool("tip")){
            new Common().showSnackBar(getWindow().getDecorView().getRootView());
        }

                if(!new Common().isMagneticSensorAvailable()){
                    new Common().showDialog(this);
                }
    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);

    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Double rawTotal;
        if(sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){

            if(new Common().readSharedPreferencesByBool("keep_wake")){
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }else{
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }

            float X_lateral = sensorEvent.values[0];
            float Y_lateral = sensorEvent.values[1];
            float Z_lateral = sensorEvent.values[2];
            //Log.d(TAG,X_lateral + "");

            rawTotal = Math.sqrt(X_lateral * X_lateral + Y_lateral * Y_lateral + Z_lateral * Z_lateral);

            BigDecimal total = new BigDecimal(rawTotal);
            double res = total.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
            String alarmLimStr = new Common().readSharedPreferencesByString("alarm_limit");

            if(!alarmLimStr.isEmpty()){
                alarmLim = Double.valueOf(alarmLimStr);
            }else {
                alarmLim = 80;
            }
            if (res < alarmLim){
                metalState.setTextColor(Color.rgb(0,0,0));
                metalState.setText("탐지중입니다.");
                int progress = (int)((res / alarmLim )* 100);
                progressView.setReachBarColor(Color.rgb(30,144,255));
                progressView.setProgress(progress);
            }else{
                metalState.setTextColor(Color.rgb(255,0,0));
                metalState.setText("발견했습니다!");
                progressView.setReachBarColor(Color.rgb(255,0,0));
                progressView.setProgress(100);

                if (new Common().isVibrate()){
                    new Common().vibrate();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onPause() {
        sensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                Intent intent = new Intent(detectActivity.this,SettingsActivity.class);
                startActivity(intent);
                break;
            default:
        }
        return true;
    }

    private void initFunc(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressView = findViewById(R.id.totalMetalProgress);
        metalState = (TextView) findViewById(R.id.metalDetect);
    }
}
