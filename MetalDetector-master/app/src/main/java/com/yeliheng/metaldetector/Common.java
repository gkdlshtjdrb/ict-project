package com.yeliheng.metaldetector;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v7.preference.PreferenceManager;
import android.view.View;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.VIBRATOR_SERVICE;


public class Common {
    boolean vibrate;
    Context context = MyApplication.getContext();


    public void vibrate(){
        Vibrator vibrator = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
        context.getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(100);
    }

    public boolean isVibrate() {
        vibrate = PreferenceManager.getDefaultSharedPreferences(
                context).getBoolean("vibrate", true);
        return vibrate;
    }


    public void writeSharedPreferencesByBool(String key,Boolean val){
        SharedPreferences sp = context.getSharedPreferences("메시지66666666666666",MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key,val);
        editor.commit();
    }



    public void writeSharedPreferencesByString(String key,String val){
        SharedPreferences sp = context.getSharedPreferences("메시지5555555",MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key,val);
        editor.commit();
    }


    public Boolean readSharedPreferencesByBool(String key){
        SharedPreferences sp = context.getSharedPreferences("메시지4",MODE_PRIVATE);
        Boolean res = sp.getBoolean(key,true);
        return res;
    }




    public String readSharedPreferencesByString(String key){
        SharedPreferences sp = context.getSharedPreferences("메시지3",MODE_PRIVATE);
        String res = sp.getString(key,"80");
        return res;
    }



    public Boolean isMagneticSensorAvailable() {
        SensorManager sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
        Sensor magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        return magneticSensor != null;
    }



















    public void showSnackBar(View view){
        Snackbar.make(view,"메시지 1",Snackbar.LENGTH_LONG).setAction("메시지1111111", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeSharedPreferencesByBool("tip",false);
            }
        }).show();
    }

    public void showDialog(Context context){
        final AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle("OOOOOOOps!")
                .setMessage("메시지2！")
                .setNegativeButton("메시지2222222", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        alertDialog.show();
    }

    }
